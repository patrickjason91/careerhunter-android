package thumbs.careerhunter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import thumbs.careerhunter.MainActivity.ClickListenerForScrolling;
import thumbs.careerhunter.MainActivity.SizeCallbackForMenu;
import thumbs.careerhunter2.R;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class LinksActivity extends Activity {

	
	   	MyHorizontalScrollView scrollView;
	    View menu;
	    View app;
	    View menulist;
	    ImageView btnSlide;
	    boolean menuOut = false;
	    Handler handler = new Handler();
	    int btnWidth;
	
	    List<String> CategoryName;
	    List<String> CategorySub;
	    HashMap<String,List<String>> ChildsData; 
	    ExpandableListAdapterWithOffsettedChild EXAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        
        LayoutInflater inflater = LayoutInflater.from(this);
        setContentView(inflater.inflate(R.layout.activity_main, null));
        
        scrollView = (MyHorizontalScrollView) findViewById(R.id.myScrollView);
        menu = findViewById(R.id.menu);
        app = inflater.inflate(R.layout.our_partner_layout, null);
        menulist = inflater.inflate(R.layout.list_items, null);
        
     

        TextView txtTitle = (TextView) findViewById(R.id.appname);
        TextView txtTitle2 = (TextView) app.findViewById(R.id.TitleTextOp);
        
       // Typeface myFont = Typeface.createFromAsset(getAssets(),"fonts/omnes_light.otf.ttf");
        
        txtTitle.setTypeface(MyFonts.setTypeface(getAssets(), MyFonts.OMNES_LIGHT));
        txtTitle2.setTypeface(MyFonts.setTypeface(getAssets(), MyFonts.OMNES_LIGHT));

        
        

       
        ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.tabBarOp);
        String[] mymenu = getResources().getStringArray(R.array.MenuArray);

     
        ListView listViewDraw = (ListView)menu;
        TextView titlebar = (TextView) app.findViewById(R.id.TitleTextOp);
        ViewUtils.initListView(this, listViewDraw,mymenu, R.layout.list_items,titlebar,scrollView,menu.getMeasuredWidth());
       
        btnSlide = (ImageView) tabBar.findViewById(R.id.BtnSlideOp);
        btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView, menu,app));

        // Create a transparent view that pushes the other views in the HSV to the right.
        // This transparent view allows the menu to be shown when the HSV is scrolled.
        View transparent = new TextView(this);
        transparent.setBackgroundColor(getResources().getColor(android.R.color.transparent));

        final View[] children = new View[] { transparent, app };
        int scrollToViewIdx = 1;
        scrollView.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(btnSlide));
        
        ExpandableListView EXList = (ExpandableListView) findViewById(R.id.exListOp);
		titlebar.setText("Links");
        prepareData();
        
        EXAdapter = new ExpandableListAdapterWithOffsettedChild(this,CategoryName,ChildsData,R.drawable.cell_bg_bot,EXList);
        
       
        EXList.setPadding(0, 5, 0, 0);
        EXList.setChildDivider(getResources().getDrawable(R.color.transparent));
        
        EXList.setAdapter(EXAdapter);
        
        
        
        EXList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
            	
            		
               return false;
            }
        });
       	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.our_partner, menu);
		return true;
	}
	
	
	private void prepareData()
	{
		CategoryName = new ArrayList<String>();
		CategorySub = new ArrayList<String>();
		ChildsData = new HashMap<String,List<String>>();
		
		CategoryName.add("Australian Government");
		CategoryName.add("Department of Human Services");
		CategoryName.add("DEEWR");
		CategoryName.add("JOB GUIDE");
		CategoryName.add("My Skills");
		CategoryName.add("Schooling");
		CategoryName.add("Youth");
		CategoryName.add("Career Information and Planning");
		CategoryName.add("Vocational Education and Training");
		CategoryName.add("Career BullŐseye Posters");
		CategoryName.add("Apprenticeships");
		CategoryName.add("Higher Education");
		CategoryName.add("Workforce Development");
		CategoryName.add("Jobs Services Australia and Disability Employment Services");
		CategoryName.add("Aboriginal and Torres Strait Islander Education Resources");
		CategoryName.add("Labour Market Resources");
		CategoryName.add("Australian Jobs");
		CategoryName.add("Business Assistance and Services");
		CategoryName.add("Early Childhood Education and Care");
	
		
		
		List<String> AG = new ArrayList<String>();
		AG.add("www.australia.gov.au");
	//	AG.add("1");
	
		List<String> DHS = new ArrayList<String>();
		DHS.add("www.humanservices.gov.au");
		//DHS.add("1");

		List<String> DEEWR = new ArrayList<String>();
		DEEWR.add("www.deewr.gov.au");
		//DEEWR.add("1");
	
		List<String> jobGuide = new ArrayList<String>();
		jobGuide.add("http://www.jobguide.deewr.gov.au/");
		//jobGuide.add("1");
	
		List<String> mySkills = new ArrayList<String>();
		mySkills.add("http://www.myskills.gov.au/");
	//	mySkills.add("1");
	
		List<String> schooling = new ArrayList<String>();
		schooling.add("http://www.myschool.edu.au/");
		//schooling.add("1");
		
		List<String> youth = new ArrayList<String>();
		youth.add("http://www.deewr.gov.au/Youth/");
	//	youth.add("1");
		
		List<String> CIP = new ArrayList<String>();
		CIP.add("http://www.myfuture.edu.au/");
		//CIP.add("1");
		
		List<String> VET = new ArrayList<String>();
		VET.add("http://training.gov.au/");
		//VET.add("1");
		
		List<String> CBP = new ArrayList<String>();
		CBP.add("http://deewr.gov.au/career-bullseye-posters");
		//CBP.add("1");
		
		List<String> apprenticeships = new ArrayList<String>();
		apprenticeships.add("http://www.australianapprenticeships.gov.au/");
		//apprenticeships.add("1");
		
		List<String> higherEd = new ArrayList<String>();
		higherEd.add("http://www.myuniversity.gov.au/");
		//higherEd.add("1");
		
		List<String> workForceDev = new ArrayList<String>();
		workForceDev.add("http://www.skillsconnect.gov.au/");
		//workForceDev.add("1");
		
		List<String> JSADES = new ArrayList<String>();
		JSADES.add("http://www.jobsearch.gov.au");
		//JSADES.add("1");
		
		List<String> ATSIER = new ArrayList<String>();
		ATSIER.add("Indigenous Pathways Website (EQ)\nhttp://indigenouspathways.eq.edu.au/wcms\nIndigenous Undergraduate Scholarships\nhttp://www.indigenousscholarships.com.au");
		//ATSIER.add("Indigenous Undergraduate Scholarships\nhttp://www.indigenousscholarships.com.au");
		//ATSIER.add("1");
		
		List<String> LabourMarkRes = new ArrayList<String>();
		LabourMarkRes.add("Labour Market Information Portal\nwww.deewr.gov.au/lmip");
		//LabourMarkRes.add("1");
		
		List<String> australianJob = new ArrayList<String>();
		australianJob.add("Australian Jobs (annual)\nwww.deewr.gov.au/australianjobs");
		//australianJob.add("1");
		
		List<String> BAS = new ArrayList<String>();
		BAS.add("Department of Industry, Innovation, Science,Research and Tertiary Education Innovation Website Directory\nhttp://www.innovation.gov.au/Pages/WebsiteDirectory.aspx");
		//BAS.add("1");
		
		List<String> ECEC = new ArrayList<String>();
		ECEC.add("http://mychild.gov.au");
		//ECEC.add("1");
	
		

		
		
		ChildsData.put(CategoryName.get(0), AG);
		ChildsData.put(CategoryName.get(1), DHS);
		ChildsData.put(CategoryName.get(2),DEEWR);
		ChildsData.put(CategoryName.get(3), jobGuide);
		ChildsData.put(CategoryName.get(4), mySkills);
		ChildsData.put(CategoryName.get(5), schooling);
		ChildsData.put(CategoryName.get(6),	 youth);
		ChildsData.put(CategoryName.get(7), CIP);
		ChildsData.put(CategoryName.get(8), VET);
		ChildsData.put(CategoryName.get(9), CBP);
		ChildsData.put(CategoryName.get(10), apprenticeships);
		ChildsData.put(CategoryName.get(11), higherEd);
		ChildsData.put(CategoryName.get(12), workForceDev);
		ChildsData.put(CategoryName.get(13), JSADES);
		ChildsData.put(CategoryName.get(14), ATSIER);
		ChildsData.put(CategoryName.get(15),LabourMarkRes);
		ChildsData.put(CategoryName.get(16), australianJob);
		ChildsData.put(CategoryName.get(17), BAS);
		ChildsData.put(CategoryName.get(18), ECEC);

		
		
		
	}

}
