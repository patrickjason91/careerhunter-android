package thumbs.careerhunter;

import thumbs.careerhunter2.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class NumInWorkForceFragment extends Fragment{

	TableLayout parentTable;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup VG,Bundle savedInstanceState)
	{
		View FragView = inflater.inflate(R.layout.number_of_workforce, VG,false);
		TextView title = (TextView) FragView.findViewById(R.id.num_of_wf_title);
	
		TextView detTitle = (TextView) FragView.findViewById(R.id.detailstitle);
		title.setTypeface(MyFonts.setTypeface(VG.getContext().getAssets(), MyFonts.OMNES_SEMIBOLD));
		detTitle.setTypeface(MyFonts.setTypeface(VG.getContext().getAssets(), MyFonts.OMNES_SEMIBOLD));
		parentTable = (TableLayout) FragView.findViewById(R.id.myTable);
		
		
		
		for(int i=0;i<13;i++)
		{
			
			
			TableRow Trow = new TableRow(FragView.getContext());
			
			for(int x=0;x<10;x++)
			{
				ImageView ImgV = new ImageView(FragView.getContext());
				ImgV.setImageResource(R.drawable.brief_case_off);
				Trow.addView(ImgV);
				
				TableRow.LayoutParams params = (TableRow.LayoutParams)ImgV.getLayoutParams();
				params.width = LayoutParams.WRAP_CONTENT;
				params.height = LayoutParams.WRAP_CONTENT;
				params.gravity = 1;
				
				
				ImgV.setPadding(3, 3, 3, 3);
				
				
			}
			
			
			parentTable.addView(Trow,new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,TableLayout.LayoutParams.WRAP_CONTENT));
		}
		
		
		
		return FragView;
	}
	
	
}
