package thumbs.careerhunter;

import java.util.HashMap;
import java.util.List;

import thumbs.careerhunter2.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ExpandableListAdapterWithOffsettedChild extends BaseExpandableListAdapter{

	private List<String> CategoryName;
	//private List<String> CategorySub;
	private HashMap<String,List<String>> ChildItems;
	private Context ActContext;
	private boolean imgIsCreated = false;
	private int lastExpandedGroupPosition;
	private ExpandableListView listView;

	
	public ExpandableListAdapterWithOffsettedChild(Context context,List<String> CategoryName,HashMap<String,List<String>> ChildLink,int BelowImage,ExpandableListView listview)
	{
		this.CategoryName = CategoryName;
		this.ChildItems = ChildLink;
		this.ActContext = context;
		this.listView = listview;
		
	}
	
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		
		
		
		return this.ChildItems.get(CategoryName.get(groupPosition)).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final String childText1 = (String) getChild(groupPosition, childPosition);
		 
		 
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.ActContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_item_layoutwithoffset, null);
        }
        
        TextView txtListChild = (TextView) convertView.findViewById(R.id.ChildText2);
        ImageView Img = new ImageView(ActContext);
    	LinearLayout ln = (LinearLayout) convertView.findViewById(R.id.ChildLayout2);
    	
    	txtListChild.setTypeface(MyFonts.setTypeface(this.ActContext.getAssets(), MyFonts.OMNES_LIGHT));
      	txtListChild.setText(childText1);
      	
        if(this.ChildItems.get(CategoryName.get(groupPosition)).size()==(childPosition+1))
        {  
        	
  
        	
      	     if(imgIsCreated == false)
      	     {
	        	Img.setImageResource(R.drawable.cell_bg_bot_child);
	        	ln.addView(Img);
	        	imgIsCreated = true;
             }
      	     
        	
        	
        }
        
        else
        {
        	txtListChild.setBackgroundResource(R.drawable.cell_bg_mid);
        	imgIsCreated = false;
       
        }
        
        
        return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		
		// TODO Auto-generated method stub
		return this.ChildItems.get(CategoryName.get(groupPosition)).size();
		
	}

	@Override
	public Object getGroup(int groupPosition) {
		
		// TODO Auto-generated method stub
		return this.CategoryName.get(groupPosition);
		
	}
	
//	public Object getGroupSub(int groupPosition)
//	{
//		return this.CategorySub.get(groupPosition);
//	}

	@Override
	public int getGroupCount() {
		
		// TODO Auto-generated method stub
		return this.CategoryName.size();
		
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@SuppressLint("NewApi")
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final String CatName = (String) getGroup(groupPosition);
		
		if(convertView == null)
		{
			LayoutInflater inflater = (LayoutInflater)this.ActContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.parent_item_layout_withoffset, null);
		}
		TextView categoryNametxt = (TextView) convertView.findViewById(R.id.categoryText);
		ImageView offsetimage = (ImageView) convertView.findViewById(R.id.parentItmCoffset);
		
		categoryNametxt.setText(CatName);
		categoryNametxt.setTypeface(MyFonts.setTypeface(ActContext.getAssets(), MyFonts.OMNES_SEMIBOLD));
		
		if(isExpanded)
		{
			offsetimage.setImageResource(R.drawable.cell_bg_mid);
		}
		else
		{			
				
				 offsetimage.setImageResource(R.drawable.cell_bg_bot);
				 
		}
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}
	
	 @Override
	 public void onGroupExpanded(int groupPosition){
	        //collapse the old expanded group, if not the same
	        //as new group to expand
	        if(groupPosition != lastExpandedGroupPosition){
	            listView.collapseGroup(lastExpandedGroupPosition);
	        }

	        super.onGroupExpanded(groupPosition);           
	        lastExpandedGroupPosition = groupPosition;
	    }

}
