package thumbs.careerhunter;


import thumbs.careerhunter.MyHorizontalScrollView.SizeCallback;
import thumbs.careerhunter2.R;
import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.viewpagerindicator.*;

/**
 * This example uses a FrameLayout to display a menu View and a HorizontalScrollView (HSV).
 * 
 * The HSV has a transparent View as the first child, which means the menu will show through when the HSV is scrolled.
 */
public class MenuActivity extends FragmentActivity{
    MyHorizontalScrollView scrollView;
    View menu;
    View app;
    View menulist;
    ImageView btnSlide;
    boolean menuOut = false;
    Handler handler = new Handler();
    int btnWidth;
    TextView pagerText;
 

    @SuppressLint("ResourceAsColor")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        
        LayoutInflater inflater = LayoutInflater.from(this);
        setContentView(inflater.inflate(R.layout.activity_menu, null));
        
        scrollView = (MyHorizontalScrollView) findViewById(R.id.myScrollView_menu);
        menu = findViewById(R.id.menu_menu);
        app = inflater.inflate(R.layout.menu_layout, null);
        menulist = inflater.inflate(R.layout.list_items, null);

        TextView txtTitle = (TextView) findViewById(R.id.appname_menu);
        TextView txtTitle2 = (TextView) app.findViewById(R.id.TitleText_menu);
        Button btnMjobs = (Button) app.findViewById(R.id.mJobs);
        Button btnTjobs = (Button) app.findViewById(R.id.tJobs);
        
        btnMjobs.setTypeface(MyFonts.setTypeface(getAssets(), MyFonts.OMNES_LIGHT_ITALIC));
        btnTjobs.setTypeface(MyFonts.setTypeface(getAssets(), MyFonts.OMNES_LIGHT_ITALIC));
       
        ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.tabBar_menu);
        String[] mymenu = getResources().getStringArray(R.array.MenuArray);
        btnSlide = (ImageView) tabBar.findViewById(R.id.BtnSlide_menu);
        btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView, menu,app));

        // Create a transparent view that pushes the other views in the HSV to the right.
        // This transparent view allows the menu to be shown when the HSV is scrolled.
        View transparent = new TextView(this);
        transparent.setBackgroundColor(android.R.color.transparent);

        final View[] children = new View[] { transparent, app };
        int scrollToViewIdx = 1;
        scrollView.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(btnSlide));
        
        ListView listViewDraw = (ListView)menu;
        TextView titlebar = (TextView) app.findViewById(R.id.TitleText_menu);
        ViewUtils.initListView(this, listViewDraw,mymenu, R.layout.list_items,titlebar,scrollView,menu.getMeasuredWidth());
        
        Typeface myFont = Typeface.createFromAsset(getAssets(),"fonts/omnes_light.otf.ttf");
        
        txtTitle.setTypeface(myFont);
        txtTitle2.setTypeface(myFont);
        
        ViewPager Vpager = (ViewPager) app.findViewById(R.id.Vpager);
        FragmentManager Fmgr = getSupportFragmentManager();
        MyPagerAdapter adapt = new MyPagerAdapter(Fmgr);
        Vpager.setAdapter(adapt);
        
        
        LinePageIndicator titleIndicator = (LinePageIndicator)app.findViewById(R.id.indicator);
        titleIndicator.setViewPager(Vpager);
        
        
        
        
        
    }
    
    public void setPagerText(String str) {
    	pagerText.setText(str);
    }
    
    static class ClickListenerForScrolling implements OnClickListener {
        HorizontalScrollView scrollView;
        View menu;
        View app;
        /**
         * Menu must NOT be out/shown to start with.
         */
        boolean menuOut = false;

        public ClickListenerForScrolling(HorizontalScrollView scrollView, View menu,View app) {
            super();
            this.scrollView = scrollView;
            this.menu = menu;
            this.app = app;
            
        }

        @SuppressLint("NewApi")
		@Override
        public void onClick(View v) {
            
            int menuWidth = menu.getMeasuredWidth();

            // Ensure menu is visible
            menu.setVisibility(View.VISIBLE);
          

            if (!menuOut) {
                // Scroll to 0 to reveal menu
                int left = 5;
                scrollView.smoothScrollTo(left, 0);
                
                
                
            } else {
                // Scroll to menuWidth so menu isn't on screen.
                int left = menuWidth;
                scrollView.smoothScrollTo(left, 0);
               
                
            }
            menuOut = !menuOut;
        }
        
        
    }
    
    static class SizeCallbackForMenu implements SizeCallback {
        int btnWidth;
        View btnSlide;

        public SizeCallbackForMenu(View btnSlide) {
            super();
            this.btnSlide = btnSlide;
        }

        @Override
        public void onGlobalLayout() {
            btnWidth = btnSlide.getMeasuredWidth();
            System.out.println("btnWidth=" + btnWidth);
        }

        @Override
        public void getViewSize(int idx, int w, int h, int[] dims) {
            dims[0] = w;
            dims[1] = h;
            final int menuIdx = 0;
            if (idx == menuIdx) {
                dims[0] = w - btnWidth;
            }
        }
    }

   
    
}



