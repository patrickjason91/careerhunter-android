package thumbs.careerhunter;

import android.content.res.AssetManager;
import android.graphics.Typeface;

public class MyFonts {
	
	public static String OMNES_LIGHT_ITALIC = "omnes_light_italic";
	public static String OMNES_LIGHT = "omnes_light";
	public static String OMNES_SEMIBOLD = "omnes_semibold";
	public static String OMNES_MEDIUM = "omnes_medium";
	
	public static Typeface setTypeface(AssetManager ast,String font)
	{
		Typeface tf = Typeface.createFromAsset(ast, "fonts/"+font+".otf.ttf");
		return tf;
	}

}
