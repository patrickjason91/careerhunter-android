package thumbs.careerhunter;

import thumbs.careerhunter2.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class AvgWeeklyWageFragment extends Fragment {
	
	TableLayout TL;
	int count;
	public View onCreateView(LayoutInflater inflater, ViewGroup VG,Bundle savedInstanceState)
	{
		count = 50;
		View Frag = inflater.inflate(R.layout.average_weekly_wage, VG,false);
		
		TL = (TableLayout) Frag.findViewById(R.id.TableContain);
		TextView weekText = (TextView) Frag.findViewById(R.id.avgWageTitle);
		weekText.setTypeface(MyFonts.setTypeface(VG.getContext().getAssets(), MyFonts.OMNES_SEMIBOLD));
		int i=0;
		do
		{
			TableRow Trow = new TableRow(VG.getContext());
			
			if(count<=4)
			{
				i = count;
			}
			else
			{
				i = 4;
			}
			
			
			for(int x =0;x<i;x++)
			{
				ImageView newImg = new ImageView(VG.getContext());
				newImg.setImageResource(R.drawable.wages_ave_dark);
				Trow.addView(newImg);
				
				TableRow.LayoutParams params = (TableRow.LayoutParams)newImg.getLayoutParams();
				params.width = LayoutParams.WRAP_CONTENT;
				params.height = LayoutParams.WRAP_CONTENT;
				params.gravity = 1;
				
				newImg.setPadding(3, 3, 3, 3);
				
			}		
			
			TL.addView(Trow,new TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
			count-=i;
		}
		while(0<count);
		
		
		return Frag;
	}
	
}
