package thumbs.careerhunter;

import thumbs.careerhunter2.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CustomListAdapter extends ArrayAdapter{

	private Context cusContext;
	private int res;
	private String[] mList;
	
	@SuppressWarnings("unchecked")
	public CustomListAdapter(Context context,
			int textViewResourceId, String[] myarray) {
		super(context, textViewResourceId, myarray);
		
		cusContext = context;
		res = textViewResourceId;
		mList = myarray;
		
	}
	
	@Override
	public View getView(int position, View v, ViewGroup parent)
    {
        View mView = v ;
        if(mView == null){
            LayoutInflater vi = (LayoutInflater)cusContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mView = vi.inflate(res, null);
        }

        TextView text = (TextView) mView.findViewById(R.id.menulist);
        

        if(mList[position] != null )
        {
        	text.setText(mList[position]);
        	text.setTypeface(MyFonts.setTypeface(cusContext.getAssets(), MyFonts.OMNES_LIGHT));
        	
        	
        	
        }

        return mView;
    }
	
	

}