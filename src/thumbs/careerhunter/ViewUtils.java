package thumbs.careerhunter;

import thumbs.careerhunter2.R;
import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Utility methods for Views.
 */
public class ViewUtils {
    private ViewUtils() {
    }

    public static void setViewWidths(View view, View[] views) {
        int w = view.getWidth();
        int h = view.getHeight();
        for (int i = 0; i < views.length; i++) {
            View v = views[i];
            v.layout((i + 1) * w, 0, (i + 2) * w, h);
            printView("view[" + i + "]", v);
        }
    }

    public static void printView(String msg, View v) {
        System.out.println(msg + "=" + v);
        if (null == v) {
            return;
        }
        System.out.print("[" + v.getLeft());
        System.out.print(", " + v.getTop());
        System.out.print(", w=" + v.getWidth());
        System.out.println(", h=" + v.getHeight() + "]");
        System.out.println("mw=" + v.getMeasuredWidth() + ", mh=" + v.getMeasuredHeight());
        System.out.println("scroll [" + v.getScrollX() + "," + v.getScrollY() + "]");
    }
    private MenuActivity MenAct;
    private static String[] mylist;
    static View myListView;
    public static void initListView(Context context, ListView listView, String prefix, int numItems, int layout) {
        // By using setAdpater method in listview we an add string array in list.
        String[] arr = new String[numItems];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = prefix + (i + 1);
        }
        listView.setAdapter(new ArrayAdapter<String>(context, layout, arr));
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = view.getContext();
                String msg = "item[" + position + "]=" + parent.getItemAtPosition(position);
                
                Toast.makeText(context, msg, 1000).show();
                System.out.println(msg);
            }
        });
    }
    
    public static void initListView(Context context, ListView listView, String[] myarray, int layout,final TextView titletext,final MyHorizontalScrollView scrollview,final int menuwidth) {
        // By using setAdpater method in listview we an add string array in list.
        
    	mylist = myarray;
    	myListView = (View)listView;
    	//ArrayAdapter listAdapter = new CustomListAdapter(context,layout,myarray);
    	
        listView.setAdapter(new CustomListAdapter(context,layout,myarray));
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               
            	Context context = view.getContext();
                String clickedMenu = (String)parent.getItemAtPosition(position);
                
                if(position != 5)
                {
	                titletext.setText(clickedMenu);
	                
	                if(mylist[position].toLowerCase().equals("menu"))
	            	{
	            		
	            			Intent inTnt = new Intent(context,MainActivity.class);
	            			inTnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            			context.startActivity(inTnt);
	            			((Activity)context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out);
	            			
	            		
	            	}
	                
	                else if(mylist[position].toLowerCase().equals("our partners"))
	                {
            			Intent inTnt = new Intent(context,OurPartnerActivity.class);
            			inTnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            			
            			context.startActivity(inTnt);
            			((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out);
	                }
	                
	                else if(mylist[position].toLowerCase().equals("links"))
	                {
            			Intent inTnt = new Intent(context,LinksActivity.class);
            			inTnt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            			
            			context.startActivity(inTnt);
            			((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out);
	                }
	                
	                
	                
	                else
	                {
	                	//titletext.setText(clickedMenu);
		                scrollview.smoothScrollTo(1000000, 0);
	                }
	                
                }
            }
        });
    }
}
