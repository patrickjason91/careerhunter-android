package thumbs.careerhunter;

import java.util.ArrayList;
import java.util.List;

/*
Copyright 2012 Aphid Mobile

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

 */
public class Menus {

  public static final List<Data> IMG_DESCRIPTIONS = new ArrayList<Data>();

  static {
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Health Care and Social Assistance", "menu_images/menu1.png",1));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Drepung Monastery", "menu_images/menu2.png",2));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Sera Monastery", "menu_images/menu3.png",3));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Samye Monastery", "menu_images/menu4.png",4));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Tashilunpo Monastery", "menu_images/menu5.png",5));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Zhangmu Port", "menu_images/menu6.png",6));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Kathmandu", "menu_images/menu7.png",7));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Pokhara", "menu_images/menu8.png",8));
                                                  
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Patan", "menu_images/menu9.png",9));
    
     Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Kathmandu", "menu_images/menu10.png",10));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Pokhara", "menu_images/menu11.png",11));
                                                  
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Patan", "menu_images/menu12.png",12));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Kathmandu", "menu_images/menu13.png",13));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Pokhara", "menu_images/menu14.png",14));
                                                  
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Patan", "menu_images/menu15.png",15));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Kathmandu", "menu_images/menu16.png",16));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Pokhara", "menu_images/menu17.png",17));
                                                  
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Patan", "menu_images/menu18.png",18));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Kathmandu", "menu_images/menu19.png",19));
    
    Menus.IMG_DESCRIPTIONS.add(new Menus.Data("Pokhara", "menu_images/menu20.png",20));
                                                  
   
    
  }

  public static final class Data {

    public final String title;
    public final String imageFilename;
    public final int tag;

    private Data(String title, String imageFilename, int tag) {
      this.title = title;
      this.imageFilename = imageFilename;
      this.tag = tag;
     
    }
  }
}
