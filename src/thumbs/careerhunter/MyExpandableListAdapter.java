package thumbs.careerhunter;

import java.util.HashMap;
import java.util.List;

import thumbs.careerhunter2.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

public class MyExpandableListAdapter extends BaseExpandableListAdapter{

	private List<Integer> ResourceList;
	private HashMap<Integer,List<String>> ChildItems;
	private Context ActContext;
	private int lastExpandedGroupPosition;
	private ExpandableListView listView;
	
	
	
	public MyExpandableListAdapter(Context context,List<Integer> Rs,HashMap<Integer,List<String>> ChildLink,ExpandableListView listview)
	{
		this.ResourceList = Rs;
		this.ChildItems = ChildLink;
		this.ActContext = context;
		this.listView = listview;
	}
	
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return this.ChildItems.get(ResourceList.get(groupPosition)).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final String childText1 = (String) getChild(groupPosition, childPosition);
		 
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.ActContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child_item_layout, null);
        }
 
        TextView txtListChild = (TextView) convertView.findViewById(R.id.ChildText);
        txtListChild.setText(childText1);
        txtListChild.setTypeface(MyFonts.setTypeface(this.ActContext.getAssets(), MyFonts.OMNES_LIGHT));
        
        return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		
		// TODO Auto-generated method stub
		return this.ChildItems.get(ResourceList.get(groupPosition)).size();
		
	}

	@Override
	public Object getGroup(int groupPosition) {
		
		// TODO Auto-generated method stub
		return this.ResourceList.get(groupPosition);
		
	}

	@Override
	public int getGroupCount() {
		
		// TODO Auto-generated method stub
		return this.ResourceList.size();
		
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final Integer resImage = (Integer) getGroup(groupPosition);
		if(convertView == null)
		{
			LayoutInflater inflater = (LayoutInflater)this.ActContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.parent_item_layout, null);
		}
		
		ImageView ParentImage = (ImageView) convertView.findViewById(R.id.parentItm);
		ParentImage.setImageResource(resImage);
		
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}
	
	 @Override
	 public void onGroupExpanded(int groupPosition){
	        //collapse the old expanded group, if not the same
	        //as new group to expand
	        if(groupPosition != lastExpandedGroupPosition){
	            listView.collapseGroup(lastExpandedGroupPosition);
	        }

	        super.onGroupExpanded(groupPosition);           
	        lastExpandedGroupPosition = groupPosition;
	    }


}
