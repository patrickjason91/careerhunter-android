package thumbs.careerhunter;

import thumbs.careerhunter2.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class TotalPercentWorkFragment extends Fragment
{
	
		public View onCreateView(LayoutInflater inflater,ViewGroup VG, Bundle savedInstanceState)
		{
			
			View Frag = inflater.inflate(R.layout.total_percentage_of_workforce_fragment,VG,false);
			
			TextView titleText = (TextView) Frag.findViewById(R.id.TotPerWFTitle);
			TextView percentText = (TextView) Frag.findViewById(R.id.TotPerWFTitle);
			
			titleText.setTypeface(MyFonts.setTypeface(VG.getContext().getAssets(), MyFonts.OMNES_SEMIBOLD));
			percentText.setTypeface(MyFonts.setTypeface(VG.getContext().getAssets(), MyFonts.OMNES_SEMIBOLD));
			
			return Frag;
		}
	
}
