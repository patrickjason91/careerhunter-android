package thumbs.careerhunter;


import java.util.ArrayList;
import java.util.List;

import thumbs.careerhunter2.R;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.aphidmobile.utils.IO;
import com.aphidmobile.utils.UI;

public class MenuAdapter extends BaseAdapter {

  private LayoutInflater inflater;
  private MainActivity Mact;
  private int repeatCount = 5;
  private List<Menus.Data> menuData;

  
  public MenuAdapter(Context context) {
    inflater = LayoutInflater.from(context);
    Mact = (MainActivity) context;
    menuData = new ArrayList<Menus.Data>(Menus.IMG_DESCRIPTIONS);
    
  }

  @Override
  public int getCount() {
    return 5;
  }

  public int getRepeatCount() {
    return repeatCount;
  }

  public void setRepeatCount(int repeatCount) {
    this.repeatCount = repeatCount;
  }

  @Override
  public Object getItem(int position) {
    return position;
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View layout = convertView;
    
    if (convertView == null) {
      layout = inflater.inflate(R.layout.menu_layout1, null);
    }

           
		    final Menus.Data data;
		    final Menus.Data data2;
		    final Menus.Data data3;
		    final Menus.Data data4;
		    
		    int pageMark = position + 1;
		    
		    if(position == 0)
		    {
			     data = menuData.get(0);
			     data2 = menuData.get(1);
			     data3 = menuData.get(2);
			     data4 = menuData.get(3);
		    }
		    else
		    {
		    	   data = menuData.get((pageMark * 4)  - 4);
		    	   data2 = menuData.get((pageMark * 4) - 3);
		    	   data3 = menuData.get((pageMark * 4) - 2);
		    	   data4 = menuData.get((pageMark * 4) - 1);
		    }
		    
		    

		    UI
		        .<ImageView>findViewById(layout, R.id.image1).setImageBitmap(IO.readBitmap(inflater.getContext().getAssets(), data.imageFilename));
		    	UI.<ImageView>findViewById(layout, R.id.image1).setOnClickListener(new View.OnClickListener() {
		          
		    		@Override
		          public void onClick(View v) {
		        	  
		        	  
		        	  Intent inT = new Intent(Mact,MenuActivity.class);
		        	  Mact.startActivity(inT);
		        	  Mact.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out);
		        	  	
		        	  	
		          }
		        });
		    
		    
		   
		    UI
		    .<ImageView>findViewById(layout, R.id.image2)
		    .setImageBitmap(IO.readBitmap(inflater.getContext().getAssets(), data2.imageFilename));
			UI.<ImageView>findViewById(layout, R.id.image2).setOnClickListener(new View.OnClickListener() {
		      @Override
		      public void onClick(View v) {
		    	  
		    	  		Intent inT = new Intent(Mact,MenuActivity.class);
		    	  		Mact.startActivity(inT);
		    	  		Mact.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out);
		      }
		    });
		    
		    
			UI
		    .<ImageView>findViewById(layout, R.id.image3)
		    .setImageBitmap(IO.readBitmap(inflater.getContext().getAssets(), data3.imageFilename));
			UI.<ImageView>findViewById(layout, R.id.image3).setOnClickListener(new View.OnClickListener() {
		      @Override
		      public void onClick(View v) {
		    	  
		    	  	Intent inT = new Intent(Mact,MenuActivity.class);
	    	  		Mact.startActivity(inT);
	    	  		Mact.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out);
		      }
		    });
		    
			UI
		    .<ImageView>findViewById(layout, R.id.image4)
		    .setImageBitmap(IO.readBitmap(inflater.getContext().getAssets(), data4.imageFilename));
			UI.<ImageView>findViewById(layout, R.id.image4).setOnClickListener(new View.OnClickListener() {
		      @Override
		      public void onClick(View v) {
		    	  
		    	  	Intent inT = new Intent(Mact,MenuActivity.class);
	    	  		Mact.startActivity(inT);
	    	  		Mact.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out);
		        
		      }
		    });
			
		   FrameLayout  fmL = (FrameLayout) layout.findViewById(R.id.footFrame);
		   TextView txt = (TextView) layout.findViewById(R.id.pager);
		   fmL.setBackgroundResource(R.drawable.footer);
		   txt.setText("Page "+(position + 1)+" of 5");
			
			

    return layout;
  }

  public void removeData(int index) {
    if (menuData.size() > 1) {
      menuData.remove(index);
    }
  }
  
}
