package thumbs.careerhunter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import thumbs.careerhunter.MainActivity.ClickListenerForScrolling;
import thumbs.careerhunter.MainActivity.SizeCallbackForMenu;
import thumbs.careerhunter2.R;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class OurPartnerActivity extends Activity {

	
	   	MyHorizontalScrollView scrollView;
	    View menu;
	    View app;
	    View menulist;
	    ImageView btnSlide;
	    boolean menuOut = false;
	    Handler handler = new Handler();
	    int btnWidth;
	
	    List<Integer> ParentData;
	    HashMap<Integer,List<String>> ChildsData; 
	    MyExpandableListAdapter EXAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        
        LayoutInflater inflater = LayoutInflater.from(this);
        setContentView(inflater.inflate(R.layout.activity_main, null));
        
        scrollView = (MyHorizontalScrollView) findViewById(R.id.myScrollView);
        menu = findViewById(R.id.menu);
        app = inflater.inflate(R.layout.our_partner_layout, null);
        menulist = inflater.inflate(R.layout.list_items, null);

        TextView txtTitle = (TextView) findViewById(R.id.appname);
        TextView txtTitle2 = (TextView) app.findViewById(R.id.TitleTextOp);
        
       // Typeface myFont = Typeface.createFromAsset(getAssets(),"fonts/omnes_light.otf.ttf");
        
        txtTitle.setTypeface(MyFonts.setTypeface(getAssets(), MyFonts.OMNES_LIGHT));
        txtTitle2.setTypeface(MyFonts.setTypeface(getAssets(), MyFonts.OMNES_LIGHT));

        
        

       
        ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.tabBarOp);
        String[] mymenu = getResources().getStringArray(R.array.MenuArray);

     
        ListView listViewDraw = (ListView)menu;
        TextView titlebar = (TextView) app.findViewById(R.id.TitleTextOp);
        ViewUtils.initListView(this, listViewDraw,mymenu, R.layout.list_items,titlebar,scrollView,menu.getMeasuredWidth());
       
        btnSlide = (ImageView) tabBar.findViewById(R.id.BtnSlideOp);
        btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView, menu,app));

        // Create a transparent view that pushes the other views in the HSV to the right.
        // This transparent view allows the menu to be shown when the HSV is scrolled.
        View transparent = new TextView(this);
        transparent.setBackgroundColor(getResources().getColor(android.R.color.transparent));

        final View[] children = new View[] { transparent, app };
        int scrollToViewIdx = 1;
        scrollView.initViews(children, scrollToViewIdx, new SizeCallbackForMenu(btnSlide));
        
        ExpandableListView EXList = (ExpandableListView) findViewById(R.id.exListOp);
		
        prepareData();
        
        EXAdapter = new MyExpandableListAdapter(this,ParentData,ChildsData,EXList);
        
       
        
        EXList.setAdapter(EXAdapter);
       	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.our_partner, menu);
		return true;
	}
	
	
	private void prepareData()
	{
		ParentData = new ArrayList<Integer>();
		ChildsData = new HashMap<Integer,List<String>>();
		
		ParentData.add(R.drawable.logo_aus_gov);
		//ParentData.add(R.drawable.logo_btn);
		ParentData.add(R.drawable.logo_busy_at_work);
		ParentData.add(R.drawable.logo_group_training);
		ParentData.add(R.drawable.logo_queensland);
		ParentData.add(R.drawable.logo_regional_dev);
		ParentData.add(R.drawable.logo_smith_fam);
		
		
		List<String> firstG = new ArrayList<String>();
		 firstG.add("The Department of Education, Employment and Workplace Relations (DEEWR) is the Australian Government department responsible for national policies and programs that enable all Australians to access quality and affordable childcare; early childhood and school education; jobs; and fair, safe and productive workplaces.\n\nThe department is also responsible for the Office for Youth which provides a coordinated approach to policies, programs and services which affect young people.\n\nThe Department of Education, Employment and Workplace Relations brings together people and services to support the Australian Government�s agenda on education, employment and workplace relations. Everyone in the department is working towards our vision of a productive and inclusive Australia.\nWebsite: http://deewr.gov.au/");
		
		
		List<String> secondG = new ArrayList<String>();
		secondG.add("BUSY At Work is a not-for-profit organization, which provides apprentice, and recruitment services to businesses.  Over the years we have established a reputation as the specialists in apprenticeship services and our five service areas provide a comprehensive approach to your apprentice needs. We provide our clients with a complete end-to-end planning approach that sets us apart from any other provider. With 35 years of experience providing skilling solutions and services to businesses, workers and job seekers in regional and metropolitan locations it�s no wonder we�ve been BUSY, but we wouldn�t have it any other way.\n\nPh. Suite 6/45 Nind Street\nSouthport QLD 4215\nPostal Address: PO BOX 303, Southport Qld 4215\nPhone: 13 28 79\nFax: (07) 5571 0192\nhttp://www.busyatwork.com.au/");
		
		
		List<String> thirdG = new ArrayList<String>();
		thirdG.add("Group Training is the largest employers of Apprentices and Trainees around Australia.  Group Training employ and find an industry placement for the Apprentice and Trainee to work in and build their experience and industry skills, whilst Group Training manages all of the training and placement requirements. \n\nFreecall: 1800 819 747\nPhone: (07) 3257 7500\nFax: (07) 3257 7577\nAddress: Unit 4 / 36 Agnes Street, Fortitude Valley, QLD 4006\nPostal: PO Box 3134, NEWSTEAD QLD 4006\nhttp://www.gtaqnt.net.au/index");
		
		List<String> fourthG = new ArrayList<String>();
		fourthG.add("The Department of Education, Training and Employment is committed to ensuring Queenslanders have the education and skills they need to contribute to the economic and social development of Queensland.\n\nThe Department delivers world-class education and training services for people at every stage of their personal and professional development. We are also committed to ensuring our education and training systems are aligned to the state's employment, skills and economic priorities.\nThe newly formed Department of Education, Training and Employment (DETE) welcomes its new Minister and Assistant Minister.\nEducation, Training and Employment Minister John-Paul Langbroek was sworn in to the Legislative Assembly of Queensland by the Queensland Governor Her Excellency Penelope Wensley AC on Tuesday 3 April 2012.\nMs Saxon Rice is Assistant Minister for Technical and Further Education.\nDr Jim Watterston is the Director-General of the Department of Education, Training and Employment\n\nTelephone: (07) 3237 0111\n30 Mary Street Brisbane Qld 4000\nDepartment of Education, Training and Employment\nPO Box 15033 City East Qld 4002\nhttp://deta.qld.gov.au/index.html");
		
		List<String> fifthG = new ArrayList<String>();
		fifthG.add("Regional Development Australia (RDA) is a partnership between the Australian, state, territory and local governments to grow and strengthen the nation's regions, and ensure their long-term sustainability.\n\n\nThere are 55 RDA Committees around Australia, including 12 in Queensland, each made up of committed local champions with a strong grass roots understanding of their regions. RDA Committees work with their communities to identify opportunities, challenges and priorities for action. They are local people developing local solutions to local issues.\n\nRDA Brisbane works within the Brisbane Local Government Area and its role is to:\n\n�	support regional planning across all levels of government\n�	work with the community to identify and take action on critical economic, social and environmental issues\n�	provide advice to local communities and government on programs and services which will help develop new opportunities in the region\n�	contribute to workforce skilling and participation strategies, innovation and technology opportunities and social inclusion strategies.\n\nRDA Brisbane builds relationships across all spheres of government, the private sector and the community. Local organisations are encouraged to work with the RDA in implementing its Regional Roadmap and addressing the six goals for Brisbane:\n\n1.	A competitive city that promotes economic growth and opportunity embracing change\n2.	A liveable city offering a high quality of life centred on social inclusion and community well-being\n3.	A clean city that is emissions conscious and values its environment\n4.	A digital city actively engaging opportunities offered by the digital economy for the benefit of all\n5.	An enterprising city with an innovative culture driven by a highly skilled and engaged workforce\n6.	A connected city with appropriate transport infrastructure and transit efficiency providing access to employment and markets.\n\nhttp://www.rdabrisbane.org.au/");
		
		List<String> sixthG = new ArrayList<String>();
		sixthG.add("The Smith Family is a national, independent children's charity helping disadvantaged Australians to get the most out of their education, so they can create better futures for themselves.\nNo one would deny that children are Australia's most vulnerable and precious resource, yet right now more than 605,000 are living in disadvantage* and are unable to access the same educational, health or life opportunities that many of us enjoy and often take for granted.  When children are left out, they get left behind and without support, the disadvantage they experience today is likely to continue into adulthood - and on to the next generation.\nThe Smith Family believes that education is the key to changing lives. As research has shown, supporting a child's education is one of the most effective ways of breaking the cycle of disadvantage and ensuring they can access the same learning opportunities as their peers.\nAt the centre of The Smith Family's work, and the heart of the organisation, is a belief in the power and possibilities of relationships.  For disadvantaged children to thrive, many of whom are growing up in lone parent and jobless households, they need to be connected to, and supported by, an extended family.\n\nhttp://www.thesmithfamily.com.au");
		

		
		
		ChildsData.put(ParentData.get(0), firstG);
		ChildsData.put(ParentData.get(1), secondG);
		ChildsData.put(ParentData.get(2),thirdG);
		ChildsData.put(ParentData.get(3), fourthG);
		ChildsData.put(ParentData.get(4), fifthG);
		ChildsData.put(ParentData.get(5), sixthG);

		
		
		
	}

}
