package thumbs.careerhunter;

import thumbs.careerhunter2.R;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AverageAgeFragment extends Fragment{
	
	int centerNum;
	LinearLayout LL;
	int Balancer;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup VG, Bundle savedInstanceState)
	{
		centerNum = 44;
		Balancer = 3;
		
		View Frag = inflater.inflate(R.layout.average_age, VG,false);
		
		LL = (LinearLayout) Frag.findViewById(R.id.avgTextcontainer);
		TextView TextTitle = (TextView)Frag.findViewById(R.id.avgAgeTitle);
		TextTitle.setTypeface(MyFonts.setTypeface(VG.getContext().getAssets(),MyFonts.OMNES_SEMIBOLD));
		
		for(int i = 6; i >= 0;i--)
		{
		    TextView Tx = new TextView(Frag.getContext());
		    Tx.setTextSize(TypedValue.COMPLEX_UNIT_SP,55);
		    Tx.setTextColor(Color.WHITE);
		    Tx.setTypeface(MyFonts.setTypeface(VG.getContext().getAssets(), MyFonts.OMNES_MEDIUM));
		    
		    if(i>3)
		    {
		    	Tx.setText(""+(centerNum + Balancer));
		    }
		    else if(i<3)
		    {
		    	Tx.setText(""+(centerNum + Balancer));
		    }
		    else
		    {
		    	Tx.setText(""+centerNum);
		    }
		    
		   /* LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) Tx.getLayoutParams();
		    params.width = LayoutParams.WRAP_CONTENT;
		    params.height = LayoutParams.WRAP_CONTENT; */
		    
		    
		    
		    LL.addView(Tx,new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT));
		    
		    
		    
		    Balancer--;
		}
		
		return Frag;
	}

}
