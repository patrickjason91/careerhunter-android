package thumbs.careerhunter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyPagerAdapter extends FragmentPagerAdapter {

	  private Fragment[] fragments = new Fragment[]{new PartByGenderFragment(),new NumInWorkForceFragment(),new TotalPercentWorkFragment(),new AverageAgeFragment(),
			  new AvgWeeklyWageFragment()};
	  
    public MyPagerAdapter(FragmentManager fm) {
         super(fm);
       
    }  

    @Override
    public Fragment getItem(int index) { 

         return fragments[index];
    }  

    @Override 
    public int getCount() {

         return 5;
    }
    
    
    @Override
    public int getItemPosition(Object object)
    {
            return POSITION_NONE;
    }
    
    
}