package thumbs.careerhunter;

import thumbs.careerhunter2.R;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class PartByGenderFragment extends Fragment{
	
	
	
	@Override  
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

	
	public View onCreateView(LayoutInflater inflater, ViewGroup VG,Bundle savedInstanceState)
	{
		View lay = inflater.inflate(R.layout.participation_by_gender_frag,VG,false);
		Context cntx = (Context) inflater.getContext();
		
		TextView TextV = (TextView) lay.findViewById(R.id.partGenTitle);
		TextView TextV2 = (TextView) lay.findViewById(R.id.malePercent);
		TextView TextV3 = (TextView) lay.findViewById(R.id.femalePercent);
		
		TextV.setTypeface(MyFonts.setTypeface(cntx.getAssets(), MyFonts.OMNES_SEMIBOLD));
		
		TextV2.setTypeface(MyFonts.setTypeface(VG.getContext().getAssets(), MyFonts.OMNES_SEMIBOLD));
		TextV3.setTypeface(MyFonts.setTypeface(VG.getContext().getAssets(), MyFonts.OMNES_SEMIBOLD));
		
		TextV3.setTextColor(Color.WHITE);
		TextV2.setTextColor(Color.WHITE);
		
		
		return lay;
	}

}
