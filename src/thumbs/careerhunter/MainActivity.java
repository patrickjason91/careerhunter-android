package thumbs.careerhunter;

import thumbs.careerhunter.MyHorizontalScrollView.SizeCallback;
import thumbs.careerhunter2.R;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * This example uses a FrameLayout to display a menu View and a
 * HorizontalScrollView (HSV).
 * 
 * The HSV has a transparent View as the first child, which means the menu will
 * show through when the HSV is scrolled.
 */
public class MainActivity extends FragmentActivity {
	MyHorizontalScrollView scrollView;
	View menu;
	View app;
	View menulist;
	ImageView btnSlide;
	boolean menuOut = false;
	Handler handler = new Handler();
	int btnWidth;
	TextView pagerText;

	@SuppressLint("ResourceAsColor")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		LayoutInflater inflater = LayoutInflater.from(this);
		setContentView(inflater.inflate(R.layout.activity_main, null));

		scrollView = (MyHorizontalScrollView) findViewById(R.id.myScrollView);
		menu = findViewById(R.id.menu);
		app = inflater.inflate(R.layout.app_layout, null);
		menulist = inflater.inflate(R.layout.list_items, null);

		TextView txtTitle = (TextView) findViewById(R.id.appname);
		TextView txtTitle2 = (TextView) app.findViewById(R.id.TitleText);
		pagerText = (TextView) app.findViewById(R.id.pager);
		// Typeface myFont =
		// Typeface.createFromAsset(getAssets(),"fonts/omnes_light.otf.ttf");

		txtTitle.setTypeface(MyFonts.setTypeface(getAssets(),
				MyFonts.OMNES_LIGHT));
		txtTitle2.setTypeface(MyFonts.setTypeface(getAssets(),
				MyFonts.OMNES_LIGHT));

		ViewGroup tabBar = (ViewGroup) app.findViewById(R.id.tabBar);
		String[] mymenu = getResources().getStringArray(R.array.MenuArray);

		ListView listViewDraw = (ListView) menu;
		TextView titlebar = (TextView) app.findViewById(R.id.TitleText);
		ViewUtils.initListView(this, listViewDraw, mymenu, R.layout.list_items,
				titlebar, scrollView, menu.getMeasuredWidth());

		btnSlide = (ImageView) tabBar.findViewById(R.id.BtnSlide);
		btnSlide.setOnClickListener(new ClickListenerForScrolling(scrollView,
				menu, app));

		// Create a transparent view that pushes the other views in the HSV to
		// the right.
		// This transparent view allows the menu to be shown when the HSV is
		// scrolled.
		View transparent = new TextView(this);
		transparent.setBackgroundColor(android.R.color.transparent);

		final View[] children = new View[] { transparent, app };
		int scrollToViewIdx = 1;
		scrollView.initViews(children, scrollToViewIdx,
				new SizeCallbackForMenu(btnSlide));

		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction atrans = fm.beginTransaction();
		Flipper frag1 = new Flipper();

		atrans.add(R.id.ContentHolder, frag1);

		atrans.commit();

	}

	public void setPagerText(String str) {
		pagerText.setText(str);
	}

	static class ClickListenerForScrolling implements OnClickListener {
		HorizontalScrollView scrollView;
		View menu;
		View app;
		/**
		 * Menu must NOT be out/shown to start with.
		 */
		boolean menuOut = false;

		public ClickListenerForScrolling(HorizontalScrollView scrollView,
				View menu, View app) {
			super();
			this.scrollView = scrollView;
			this.menu = menu;
			this.app = app;

		}

		@Override
		public void onClick(View v) {

			int menuWidth = menu.getMeasuredWidth();

			// Ensure menu is visible
			menu.setVisibility(View.VISIBLE);
			// LinearLayout lout = (LinearLayout)app.findViewById(R.id.app);

			if (!menuOut) {
				// Scroll to 0 to reveal menu
				int left = 5;
				scrollView.smoothScrollTo(left, 0);

			} else {
				// Scroll to menuWidth so menu isn't on screen.
				int left = menuWidth;
				scrollView.smoothScrollTo(left, 0);

			}
			menuOut = !menuOut;
		}

	}

	static class SizeCallbackForMenu implements SizeCallback {
		int btnWidth;
		View btnSlide;

		public SizeCallbackForMenu(View btnSlide) {
			super();
			this.btnSlide = btnSlide;
		}

		@Override
		public void onGlobalLayout() {
			btnWidth = btnSlide.getMeasuredWidth();
			System.out.println("btnWidth=" + btnWidth);
		}

		@Override
		public void getViewSize(int idx, int w, int h, int[] dims) {
			dims[0] = w;
			dims[1] = h;
			final int menuIdx = 0;
			if (idx == menuIdx) {
				dims[0] = w - btnWidth;
			}
		}
	}

}
